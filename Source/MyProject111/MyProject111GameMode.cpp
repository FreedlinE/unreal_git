// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyProject111GameMode.h"
#include "MyProject111HUD.h"
#include "MyProject111Character.h"
#include "UObject/ConstructorHelpers.h"

AMyProject111GameMode::AMyProject111GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMyProject111HUD::StaticClass();
}
