// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyProject111GameMode.generated.h"

UCLASS(minimalapi)
class AMyProject111GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMyProject111GameMode();
};



